import { Component, OnInit } from '@angular/core';
import { EstadisticasService } from '../services/estadisticas.service';
import { Observable } from 'rxjs';
import { User } from './user';

@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.page.html',
  styleUrls: ['./estadisticas.page.scss'],
})
export class EstadisticasPage implements OnInit {

  usuarios : User[] = [];
  usuario : User;

  constructor(public http: EstadisticasService) {
    this.http.loadInfo().subscribe(
      (res) => {
        for(let value  of res) {
          this.usuario = {  
            nombre : value[0],
            rank : value[2],
            score : value[3],
            wus : value[4]
          }
          this.usuarios.push(this.usuario);
          if(this.usuarios.length == 11){
            this.usuarios.splice(0,1);
            for(let usu of this.usuarios){
              usu.nombre = usu.nombre.split("_",1)[0];
            }
            break;
          }
        };
      },
      (error) =>{
        console.error(error);
      }
    )
  }

  ngOnInit() {}

}
