import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EstadisticasService {

  proxyurl : string = "https://cors-anywhere.herokuapp.com/";
  path : string = 'https://api.foldingathome.org/team/244686/members'; //'https://api.foldingathome.org/team/224497/recent/wus'; //'https://api.foldingathome.org/team/224497/accounts'; 
  

  constructor(private http: HttpClient) { }

  loadInfo() : Observable<any> {
    return this.http
    .get(this.proxyurl + this.path)
  }
}
